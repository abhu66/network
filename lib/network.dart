import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class NetworkClass extends Interceptor {

   Future<Response> doPost({required String url, dynamic body,dynamic header})async {
     var response = await dio.post(url,data: body,options: Options(headers: header));
     return response;
  }

  Future<Response> doGet({required String url,dynamic header}) async {

   var response = await dio.get(url,options: Options(headers: header));
   return response;
  }

  Future<Response?> doPut({required String url,dynamic body,dynamic header,})async {
    Response? response;
    return response;
  }

  Future<Response?> doDelete({required String url,dynamic body,dynamic header,})async {
    Response? response;
    return response;
  }
}

class Interceptor{
  Dio dio = Dio();
  Interceptor(){
    if(kDebugMode){
      dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: true,
        compact: false,
      )
      );
    }
  }
}
